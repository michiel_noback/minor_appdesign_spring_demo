# Spring Boot web mvc demo project #

This is a Spring Boot demo project for use in the course Application design.
It demonstrates basic structure and setup of the project and essential use case scenarios. 

### Features ###

* Basic structure
* Request handling, including path variables and redirects
* Model attributes, Flash Attributes
* Form data and submit handling and verification
* DataSources & Services
* Database (MySQL/JDBC) access
* Internationalization using URL 
* Thymeleaf templating
* Profiles
* Error handling basics

### How do I get set up? ###

* Clone the repo (into your IntellijIdeaProjects folder)
* Import the project using the `build.gradle` file
* Change the `application.properties` file to reflect your situation
* Use Gradle build configurations to run the main class

### Who do I talk to? ###

* Your teacher
