package nl.bioinf.nomi.my_first_spring_boot_project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

/**
 * Created by michiel on 22/03/2017.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * The configured datasource
     */
    @Autowired
    private DataSource dataSource;

    /**
     * This config makes home, login, /LOCALE/home and /LOCALE/greeting available to everyone, as well as all styling, images and javascript
     * @param httpSecurity
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                    .antMatchers("/", "/home", "/login", "/*/greeting", "/*/home",
                            "/images/**", "/css/**", "/js/**", "/desktop/**").permitAll()//, "/**/home"
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .defaultSuccessUrl("/home")
                    .and()
                /*The mapping /logout will automatically log the user out, without any html page. Will redirect to /login?logout*/
                .logout()
                    .permitAll();
    }

    /**
     * A simple inmemory authenticator checks for username michiel and password michiel
     * @param auth
     * @throws Exception
     */
//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .inMemoryAuthentication()
//                .withUser("michiel").password("michiel").roles("USER");
//    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withUsername("michiel").password("michiel").roles("USER").build());
////        manager.createUser(User.withUsername("admin").password("password").roles("USER","ADMIN").build());
//        return manager;
//    }

    /**
     * The password encoder. Uses a {@link BCryptPasswordEncoder} to encode the password for extra security.
     *
     * @return the password encoder
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * Database security configuration for the login system.
     *
     * @param auth            authentication manager
     * @param passwordEncoder the password encoder
     * @throws Exception the exception
     */
    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth, PasswordEncoder passwordEncoder) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(passwordEncoder)
                .usersByUsernameQuery(
                        "select email, password, enabled from users where email = ? and enabled = true")
                .authoritiesByUsernameQuery(
                        "select email, authority from users where email = ?");
    }
}