package nl.bioinf.nomi.my_first_spring_boot_project.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
//@EnableWebMVC //leaving this annotation breaks the default locations
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/desktop/**")
                .addResourceLocations("file:/Users/michiel/Desktop/");
//        registry
//                .addResourceHandler("/resources/**")
//                .addResourceLocations("classpath:/static/**");
    }
}
