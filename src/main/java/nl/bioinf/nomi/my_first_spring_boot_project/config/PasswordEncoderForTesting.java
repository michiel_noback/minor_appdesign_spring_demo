package nl.bioinf.nomi.my_first_spring_boot_project.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoderForTesting {


    public static void main(String[] args) {
//        String password = "zwanie";
        String password = "henkie";

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        System.out.println(hashedPassword);
    }


}
