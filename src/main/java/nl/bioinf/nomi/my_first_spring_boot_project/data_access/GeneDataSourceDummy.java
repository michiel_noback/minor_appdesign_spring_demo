package nl.bioinf.nomi.my_first_spring_boot_project.data_access;

import nl.bioinf.nomi.my_first_spring_boot_project.model.Gene;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Created by michiel on 04/03/2017.
 */
@Component
@Profile("dev")
public class GeneDataSourceDummy implements GeneDataSource {
    @Override
    public Gene getGeneById(String idPattern) {
        return null;
    }

    @Override
    public Gene getGeneByName(String namePattern) {
        return null;
    }

    @Override
    public Gene getGeneByAbbreviation(String abbreviation) {
        Gene g = new Gene();
        g.setAbbreviation("DEV_GENE");
        g.setName("Development-only gene name");
        return g;
    }
}
