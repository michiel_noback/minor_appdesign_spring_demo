package nl.bioinf.nomi.my_first_spring_boot_project.data_access;

import nl.bioinf.nomi.my_first_spring_boot_project.model.Gene;

/**
 * Created by michiel on 01/03/2017.
 */
public interface GeneDataSource {
    Gene getGeneById(String idPattern);

    Gene getGeneByName(String namePattern);

    Gene getGeneByAbbreviation(String abbreviation);
}
