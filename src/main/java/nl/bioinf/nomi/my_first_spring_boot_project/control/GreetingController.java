package nl.bioinf.nomi.my_first_spring_boot_project.control;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by michiel on 14/02/2017.
 */
@Controller
public class GreetingController {
    @RequestMapping("/{locale}/greeting")
    public String greeting(
            @RequestParam(
                    value = "name",
                    required = false,
                    defaultValue = "Michilio Tuur")
                    String name,
            @PathVariable("locale") String locale,
            Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }


}
