package nl.bioinf.nomi.my_first_spring_boot_project.control;

import nl.bioinf.nomi.my_first_spring_boot_project.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.Locale;

/**
 * Created by michiel on 15/02/2017.
 */
@Controller
public class HomeController {
    @Value("${myappl.admin-name}")
    private String adminName;


    @RequestMapping(
            value = {"", "/", "/home"}, method = RequestMethod.GET)
    public String home(
            Model model,
            Locale locale) {
        return "redirect:" + locale.getLanguage() + "/home";
    }

    @RequestMapping(value = "/{locale}/home")
    public String homeWithLocale(Model model, Authentication authentication) {
        if (authentication != null) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            System.out.println("User has authorities: " + userDetails.getAuthorities());
            System.out.println("User has name: " + userDetails.getUsername());
            //etc check it out!
        }

        //much more you can do with authentication, userdetails and Principal
        //see: https://www.baeldung.com/get-user-in-spring-security
        System.out.println("adminName = " + adminName);

        //check if user exists
        if (! model.containsAttribute("user")) {
            model.addAttribute(User.getGuest());
        }

        return "index";
    }
}
