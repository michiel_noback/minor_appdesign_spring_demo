package nl.bioinf.nomi.my_first_spring_boot_project.model;

/**
 * Created by michiel on 01/03/2017.
 */
public class Gene {
    //(genbank_id, abbreviation, name, process)
    private int id;
    private String genbankId;
    private String abbreviation;
    private String name;
    private String process;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGenbankId() {
        return genbankId;
    }

    public void setGenbankId(String genbankId) {
        this.genbankId = genbankId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public Gene(int id, String genbankId, String abbreviation, String name, String process) {
        this.id = id;
        this.genbankId = genbankId;
        this.abbreviation = abbreviation;
        this.name = name;
        this.process = process;
    }

    public Gene() {
    }
}
