package nl.bioinf.nomi.my_first_spring_boot_project.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by michiel on 27/02/2017.
 */
public class FirstForm {
    @NotNull
    @Min(1)
    private long id;
    @NotNull
    @Size(min = 2, max = 10)
    private String content;
    @NotNull
    private String email;

    public FirstForm() {
        this.id = 23;
        this.content = "default content";
        this.email = "someone@example.org";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
